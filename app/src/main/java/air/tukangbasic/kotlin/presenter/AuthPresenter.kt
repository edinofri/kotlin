package air.tukangbasic.kotlin.presenter

import air.tukangbasic.kotlin.helpers.AuthResponse
import android.content.Context

import java.util.HashMap

import air.tukangbasic.kotlin.helpers.ConnectionAPI
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.SessionManager
import air.tukangbasic.kotlin.models.basic.Response
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by root on 16/04/18.
 */

class AuthPresenter(private val context: Context, private var listener: AuthResponse) {

    fun doLogin(email: String, password: String) {
        val data = HashMap<String, String>()
        data[Constant.KEY_EMAIL] = email
        data[Constant.KEY_PASSWORD] = password

        ConnectionAPI.getInstance(this.context)
                .api
                .login(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                                SessionManager(this.context!!).setUserLogin(data.body()?.data?.user, data.body()?.data?.token)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error -> listener.onFailure(error.message) }
                )
    }

    fun doRegister(name: String, email: String, password: String) {
        val data = HashMap<String, String>()
        data.put(Constant.KEY_NAME, name)
        data.put(Constant.KEY_EMAIL, email)
        data.put(Constant.KEY_PASSWORD, password)

        ConnectionAPI.getInstance(this.context)
                .api
                .register(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                                SessionManager(this.context!!).setUserLogin(data.body()?.data?.user, data.body()?.data?.token)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error -> listener.onFailure(error.message) }
                )
    }

    fun getAddresses() {
        ConnectionAPI.getInstance(context)
                .api
                .getAddress()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )
    }

    fun addNewAddress(data: Map<String, String>) {
        ConnectionAPI.getInstance(context)
                .api
                .addAddress(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )
    }

    fun deleteAddress() {}

}
