package air.tukangbasic.kotlin.presenter

import air.tukangbasic.kotlin.helpers.BaseResponse
import air.tukangbasic.kotlin.helpers.ConnectionAPI
import air.tukangbasic.kotlin.models.basic.Response
import android.content.Context
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.collections.HashMap

/**
 * Created by tukangbasic on 15/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class ProductPresenter(private val context: Context, private var listener: BaseResponse) {

    fun getProducts() {
        ConnectionAPI.getInstance(context)
                .api
                .products()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error -> listener.onFailure(error.message) }
                )
    }

    fun addToCart(product_id: String, qty: String) {
        var map: HashMap<String, String> = HashMap()
        map["product_id"] = product_id
        map["qty"] = qty

        ConnectionAPI.getInstance(context)
                .api
                .addCart(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )

    }

    fun getCart() {
        ConnectionAPI.getInstance(context)
                .api
                .getCart()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )
    }

    fun doCheckout(address_id: Int) {
        var map: HashMap<String, String> = HashMap()
        map["address_id"] = address_id.toString()

        ConnectionAPI.getInstance(context)
                .api
                .checkout(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )
    }

    fun getHistory(page: Int = 1) {
        ConnectionAPI.getInstance(context)
                .api
                .getHistory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { data ->
                            if (data.code() == 200) {
                                listener.onSuccess(data.body()!!)
                            } else {
                                listener.onFailure(Gson().fromJson(data.errorBody().toString(), Response::class.java)?.message)
                            }
                        },
                        { error ->
                            listener.onFailure(error.message)
                        }
                )
    }
}