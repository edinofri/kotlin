package air.tukangbasic.kotlin.views.adapters

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.EventBusHelper

import air.tukangbasic.kotlin.models.basic.Product
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.greenrobot.eventbus.EventBus

/**
 * Created by tukangbasic on 19/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class ProductAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var action: (item:Product) -> Unit
    var items: List<Product> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return Holder(LayoutInflater.from(parent!!.context).inflate(R.layout.item_product, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        var h = holder as Holder
        Picasso.get().load(Constant.BASE_IMAGES_URL + Constant.PATH_PRODUCT + items[position].image).fit().into(h.image)
        h.text_title.text = items[position].name
        h.text_price.text = String.format("%sK", items[position].price.toInt() / 1000)
        h.text_category.text = items[position].category.name
        h.text_label.text = String.format("Stock : %s", items[position].stock)
    }

    fun onSelect(action: (item:Product) -> Unit) {
        this.action = action
    }

    inner class Holder(v: View) : RecyclerView.ViewHolder(v) {
        var image = v.findViewById<ImageView>(R.id.image)
        var text_category = v.findViewById<TextView>(R.id.category)
        var text_title = v.findViewById<TextView>(R.id.title)
        var text_price = v.findViewById<TextView>(R.id.price)
        var text_label = v.findViewById<TextView>(R.id.label)

        init {
            v.setOnClickListener {
                action(items[adapterPosition])
            }
        }
    }
}