package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.AuthResponse
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.models.basic.Error
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.presenter.AuthPresenter
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_address.*
import java.util.HashMap

class AddAddressActivity : AppCompatActivity(), AuthResponse {

    var data: HashMap<String, String> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_save.click {
            var message: String = if (alamat.text.toString().isEmpty()) "Alamat harus di isi" else ""
            message = if (phone_number.text.toString().isEmpty()) "No. Telepon harus di isi" else message
            message = if (receiver_name.text.toString().isEmpty()) "Nama penerima harus di isi" else message
            if (!message.isEmpty()) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                return@click
            }

            data["receiver_name"] = receiver_name.text.toString()
            data["phone_number"] = phone_number.text.toString()
            data["provinsi"] = provinsi.text.toString()
            data["kabupaten"] = kabupaten.text.toString()
            data["kecamatan"] = kecamatan.text.toString()
            data["alamat"] = alamat.text.toString()


            btn_save.text = ""
            progressbar.visibility = View.VISIBLE

            AuthPresenter(applicationContext, this).addNewAddress(data)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccess(response: Response) {
        btn_save.text = "SIMPAN"
        progressbar.visibility = View.INVISIBLE
        setResult(Activity.RESULT_OK)
        Toast.makeText(applicationContext,"Berhasil membuat Alamat baru",Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onErrors(errors: List<Error>) {
    }

    override fun onFailure(message: String?) {
        btn_save.text = "SIMPAN"
        progressbar.visibility = View.INVISIBLE
        Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
    }

}
