package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.toHumanDate
import air.tukangbasic.kotlin.helpers.toPrice
import air.tukangbasic.kotlin.models.basic.CartItem
import air.tukangbasic.kotlin.models.basic.History
import air.tukangbasic.kotlin.views.adapters.CartAdapter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_history_detail.*

class HistoryDetailActivity : AppCompatActivity() {

    lateinit var data: History
    var adapter: CartAdapter = CartAdapter(true)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        data = Gson().fromJson(intent.getStringExtra("data"), History::class.java)

        status.text = data.getStatus()
        status.setTextColor(data.getColorByStatus())
        date.text = data.created_at.toHumanDate()
        invoice.text = data.invoice
        receiver_name.text = data.address.receiver_name
        phone_number.text = data.address.phone_number
        full_address.text = data.address.alamat + ", " + listOf(data.address.kecamatan, data.address.kabupaten, data.address.provinsi).joinToString(", ")
        total_qty.text = data.cart.items.map { it.qty }.sum().toString() + " pcs"
        total_price.text = "Rp. " + data.cart.items.map { it.total }.sum().toPrice()
        total_sum_price.text = "Rp. " + data.cart.items.map { it.total }.sum().toPrice()

        recyclerview.setHasFixedSize(true)
        recyclerview.layoutManager = LinearLayoutManager(applicationContext)
        recyclerview.adapter = adapter

        adapter.items = data.cart.items as MutableList<CartItem>
        adapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
