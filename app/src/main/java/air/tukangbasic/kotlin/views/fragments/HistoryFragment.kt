package air.tukangbasic.kotlin.views.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.BaseResponse
import air.tukangbasic.kotlin.models.basic.History
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.models.response.ResponseHistory
import air.tukangbasic.kotlin.presenter.ProductPresenter
import air.tukangbasic.kotlin.views.adapters.HistoryAdapter
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_history.*


class HistoryFragment : Fragment(), BaseResponse {
    var adapter: HistoryAdapter = HistoryAdapter()
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerview.getIt().setHasFixedSize(true)
        recyclerview.getIt().layoutManager = LinearLayoutManager(context)
        recyclerview.getIt().adapter = adapter

        loadData()
        swiperefresh.setOnRefreshListener {
            ProductPresenter(context, this).getHistory()
        }

    }

    private fun loadData() {
        recyclerview.loading = true
        ProductPresenter(context, this).getHistory()
    }

    override fun onSuccess(response: Response) {
        swiperefresh.isRefreshing = false
        recyclerview.loading = false

        response as ResponseHistory


        adapter.items = response.data?.data as MutableList<History>
        recyclerview.message = "No Data"
        recyclerview.isEmpty = adapter.items.size == 0

        adapter.notifyDataSetChanged()

    }

    override fun onFailure(message: String?) {
        swiperefresh.isRefreshing = false
        recyclerview.loading = false

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}// Required empty public constructor
