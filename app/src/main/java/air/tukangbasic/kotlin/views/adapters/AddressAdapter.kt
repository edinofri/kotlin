package air.tukangbasic.kotlin.views.adapters

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.models.basic.Address
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson

/**
 * Created by tukangbasic on 10/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class AddressAdapter(val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var context: Context
    var items: MutableList<Address> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        context = parent!!.context
        return Holder(LayoutInflater.from(context).inflate(R.layout.item_address, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        holder as Holder
        holder.receiver_name.text = items[position].receiver_name
        holder.phone_number.text = items[position].phone_number
        holder.alamat.text = items[position].alamat
        holder.more.text = listOf(items[position].kecamatan, items[position].kabupaten, items[position].provinsi).joinToString(", ")
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var receiver_name: TextView = view.findViewById(R.id.receiver_name)
        var phone_number: TextView = view.findViewById(R.id.phone_number)
        var alamat: TextView = view.findViewById(R.id.alamat)
        var btn_area: LinearLayout = view.findViewById(R.id.selected_area)
        var more: TextView = view.findViewById(R.id.more)

        init {
            btn_area.click {
                var intent = Intent()
                intent.putExtra(Constant.KEY_DATA, Gson().toJson(items[adapterPosition]))
                activity.setResult(Activity.RESULT_OK, intent)
                activity.finish()
            }
        }
    }
}