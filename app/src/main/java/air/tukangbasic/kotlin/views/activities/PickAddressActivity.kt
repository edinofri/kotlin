package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.AuthResponse
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.models.basic.Error
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.models.response.ResponseAddress
import air.tukangbasic.kotlin.presenter.AuthPresenter
import air.tukangbasic.kotlin.views.adapters.AddressAdapter
import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_pick_address.*

class PickAddressActivity : AppCompatActivity(), AuthResponse {

    var adapter: AddressAdapter = AddressAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_address)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        progressbar_stick.isIndeterminate = true

        recyclerview.setHasFixedSize(true)
        recyclerview.layoutManager = LinearLayoutManager(applicationContext)
        recyclerview.adapter = adapter

        loadData()

        btn_add_address.click {
            startActivityForResult(Intent(applicationContext, AddAddressActivity::class.java), Constant.REQUEST_CODE_ADD_ADDRESS)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REQUEST_CODE_ADD_ADDRESS && resultCode == Activity.RESULT_OK) {
            loadData()
        }
    }

    private fun loadData() {
        progressbar_stick.visibility = View.VISIBLE
        AuthPresenter(applicationContext, this).getAddresses()
    }

    override fun onSuccess(response: Response) {
        progressbar_stick.visibility = View.GONE
        response as ResponseAddress
        adapter.items = response.data
        adapter.notifyDataSetChanged()
    }

    override fun onErrors(errors: List<Error>) {
    }

    override fun onFailure(message: String?) {
        progressbar_stick.visibility = View.GONE
    }

}
