package air.tukangbasic.kotlin.views.fragments

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.BaseResponse
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.EventBusHelper
import air.tukangbasic.kotlin.helpers.SessionManager
import air.tukangbasic.kotlin.models.basic.Product
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.presenter.ProductPresenter
import air.tukangbasic.kotlin.views.activities.MainActivity
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bottom_sheet_product_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by tukangbasic on 19/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class ProductDetailBottomSheetFragment : BottomSheetDialogFragment(), BaseResponse {

    lateinit var product: Product
    var qty: Int = 0
    var isLoading = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.bottom_sheet_product_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get().load(Constant.BASE_IMAGES_URL + Constant.PATH_PRODUCT + product.image).fit().into(image)
        title.text = product.name
        price.text = String.format("Rp. %s", product.price.toInt())
        category.text = product.category.name
        description.text = Html.fromHtml(product.description)

        main_container.setOnClickListener {
            description_container.visibility = if (description_container.visibility == View.GONE) View.VISIBLE else View.GONE
        }

        btn_add.setOnClickListener {
            if (qty < product.stock) {
                qty += 1
                input_qty.setText(qty.toString())

            }
        }

        btn_remove.setOnClickListener {
            if (qty > 0) {
                qty -= 1
                input_qty.setText(qty.toString())

            }
        }

        btn_add_to_cart.setOnClickListener {
            if (!SessionManager(context).isLogin()) {
                Toast.makeText(context,"Login terlebih dahulu",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!isLoading) {
                btn_add_to_cart.setTextColor(Color.TRANSPARENT)
                progressbar.visibility = View.VISIBLE
                ProductPresenter(context, this).addToCart(product.id.toString(), qty.toString())
                isLoading = true
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        qty = 0
        input_qty.setText(qty.toString())
    }

    override fun onSuccess(response: Response) {
        isLoading = false
        (activity as MainActivity).refreshDataBeranda()
        progressbar.visibility = View.INVISIBLE
        btn_add_to_cart.setTextColor(Color.WHITE)
        this.dismissAllowingStateLoss()
        Toast.makeText(context, "success update cart", Toast.LENGTH_SHORT).show()

    }

    override fun onFailure(message: String?) {
        isLoading = false
        progressbar.visibility = View.INVISIBLE
        btn_add_to_cart.setTextColor(Color.WHITE)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}