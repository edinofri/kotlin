package air.tukangbasic.kotlin.views.adapters

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.helpers.toDate
import air.tukangbasic.kotlin.helpers.toHumanDate
import air.tukangbasic.kotlin.models.basic.History
import air.tukangbasic.kotlin.views.activities.HistoryDetailActivity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var items: MutableList<History> = mutableListOf()
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        context = parent!!.context
        return Holder(LayoutInflater.from(context).inflate(R.layout.item_history, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        holder as Holder
        holder.invoice.text = items[position].invoice
        holder.date.text = "Order date : " + items[position].created_at.toHumanDate()
        holder.status.text = items[position].getStatus()
        holder.status.background = items[position].getDrawableByStatus(context)
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var invoice: TextView = view.findViewById(R.id.invoice)
        var date: TextView = view.findViewById(R.id.date)
        var status: TextView = view.findViewById(R.id.status)

        init {
            view.click {
                context.startActivity(Intent(context, HistoryDetailActivity::class.java)
                        .putExtra("data", Gson().toJson(items[adapterPosition])))
            }
        }
    }
}
