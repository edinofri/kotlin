package air.tukangbasic.kotlin.views.dialog

import air.tukangbasic.kotlin.R
import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager

/**
 * Created by tukangbasic on 16/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class PrimitiveDialog(c: Context, title: String?, listener: AddActions?)
    : Dialog(c) {

    init {
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_primitive);
    }

    interface AddActions {
        fun onDismiss()
        fun onSubmit()
    }
}