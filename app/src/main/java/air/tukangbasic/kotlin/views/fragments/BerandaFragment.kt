package air.tukangbasic.kotlin.views.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.*
import air.tukangbasic.kotlin.models.basic.CartItem
import air.tukangbasic.kotlin.models.basic.Product
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.models.response.ResponseCart
import air.tukangbasic.kotlin.models.response.ResponseProduct
import air.tukangbasic.kotlin.presenter.ProductPresenter
import air.tukangbasic.kotlin.views.activities.CartActivity
import air.tukangbasic.kotlin.views.activities.MainActivity
import air.tukangbasic.kotlin.views.adapters.ProductAdapter
import android.app.Activity
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_beranda.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class BerandaFragment : Fragment(), BaseResponse {

    var adapter: ProductAdapter = ProductAdapter()
    var productDetailBottomSheetFragment: ProductDetailBottomSheetFragment = ProductDetailBottomSheetFragment()

    var listCart: MutableList<CartItem> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_beranda, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onLogin()
        refreshCart()
        recyclerview.getIt().setHasFixedSize(true)
        recyclerview.getIt().layoutManager = LinearLayoutManager(context)
        recyclerview.getIt().adapter = adapter
        recyclerview.getIt().isNestedScrollingEnabled = false

        loadData()
        swiperefresh.setOnRefreshListener {
            ProductPresenter(context, this).getProducts()
        }
        adapter.onSelect { item: Product ->
            run {
                productDetailBottomSheetFragment.product = item
                productDetailBottomSheetFragment.show(fragmentManager, Constant.TAG_PRODUCT_DETAIL_BOTTOM_SHEET)
            }

        }
        btn_cart.click {
            startActivityForResult(Intent(context, CartActivity::class.java).putExtra("data", Gson().toJson(listCart)), Constant.REQUEST_CODE_ORDER_SUCCESS)
        }
    }

    fun loadData() {
        recyclerview.loading = true
        ProductPresenter(context, this).getProducts()
    }

    fun onLogin() {
        if (SessionManager(context).isLogin()) {
            card_user.visibility = View.VISIBLE
            user_name.text = SessionManager(context).getUser().name
            email.text = SessionManager(context).getUser().email
            space_divider.visibility = View.VISIBLE
        } else {
            card_user.visibility = View.GONE
            space_divider.visibility = View.GONE
        }
    }

    fun refreshCart() {
        if (SessionManager(context).isLogin()) {
            ProductPresenter(context, object : BaseResponse {
                override fun onSuccess(response: Response) {
                    response as ResponseCart
                    listCart = response.data!!

                    if (listCart.size == 0) {
                        count_item.visibility = View.INVISIBLE
                    } else {
                        count_item.visibility = View.VISIBLE
                    }
                    count_item.text = listCart.size.toString()

                }

                override fun onFailure(message: String?) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                }

            }).getCart()
        }
    }

    override fun onSuccess(response: Response) {
        swiperefresh.isRefreshing = false
        recyclerview.loading = false

        response as ResponseProduct
        adapter.items = response.data?.data!!
        recyclerview.isEmpty = response.data?.data!!.isEmpty()
        recyclerview.message = "No Data"

        adapter.notifyDataSetChanged()
    }

    override fun onFailure(message: String?) {
        swiperefresh.isRefreshing = false
        recyclerview.loading = false
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onReceive(data: EventBusHelper.AuthEvent) {
        onLogin()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRefresh(data: EventBusHelper.RefreshBeranda) {
        refreshCart()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE_ORDER_SUCCESS) {
            refreshCart()
            (activity as MainActivity).goTo(R.id.nav_history)
        }
    }
}// Required empty public constructor
