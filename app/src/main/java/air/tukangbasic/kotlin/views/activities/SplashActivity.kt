package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.SessionManager
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    private val SPLASH_SCREEN = 3000L
    private var handler: Handler? = null

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val target = if (SessionManager(applicationContext).isLogin()) MainActivity::class.java else LoginActivity::class.java
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        handler = Handler()
        handler!!.postDelayed(mRunnable, SPLASH_SCREEN)

    }
}
