package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.BaseResponse
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.helpers.toPrice
import air.tukangbasic.kotlin.models.basic.Address
import air.tukangbasic.kotlin.models.basic.CartItem
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.presenter.ProductPresenter
import air.tukangbasic.kotlin.views.adapters.CartAdapter
import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity() {
    var listCart: MutableList<CartItem> = mutableListOf()
    var adapter: CartAdapter = CartAdapter()
    var address_id: Int = 0
    var geo: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        listCart = Gson().fromJson(intent.getStringExtra("data"), object : TypeToken<List<CartItem>>() {}.type)

        recyclerview.setHasFixedSize(true)
        recyclerview.layoutManager = LinearLayoutManager(applicationContext)
        recyclerview.adapter = adapter

        updateView()

        btn_order.click {
            if (address_id == 0) {
                Toast.makeText(applicationContext, "Address is Required", Toast.LENGTH_SHORT).show()
                return@click
            }

            if(listCart == null || listCart.size == 0){
                Toast.makeText(applicationContext, "Cart is Empty", Toast.LENGTH_SHORT).show()
                return@click
            }
            ProductPresenter(applicationContext, object : BaseResponse {
                override fun onSuccess(response: Response) {
                    Toast.makeText(applicationContext, "Success order", Toast.LENGTH_SHORT).show()
                    setResult(Activity.RESULT_OK)
                    finish()
                }

                override fun onFailure(message: String?) {
                    Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                }
            }).doCheckout(address_id)
        }
        btn_pick_address.click {
            startActivityForResult(Intent(applicationContext, PickAddressActivity::class.java), Constant.REQUEST_CODE_PICK_ADDRESS)
        }
    }

    private fun updateView() {
        adapter.items = listCart
        adapter.notifyDataSetChanged()
        total.text = "Total : Rp. " + listCart.map { it.total }.sum().toPrice()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REQUEST_CODE_PICK_ADDRESS && resultCode == Activity.RESULT_OK) {
            val address = Gson().fromJson(data?.getStringExtra(Constant.KEY_DATA), Address::class.java)
            full_address.text = address.alamat + ", "+ listOf(address.kecamatan,address.kabupaten,address.provinsi).joinToString ( ", " )
            this.address_id = address.id
        }
    }
}
