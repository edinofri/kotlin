package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.AuthResponse
import air.tukangbasic.kotlin.models.basic.Error
import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.presenter.AuthPresenter
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), AuthResponse {

    private lateinit var presenter: AuthPresenter
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        presenter = AuthPresenter(this, this)
        setListener()
    }

    private fun setListener() {
        btn_login.setOnClickListener {
            btn_login.text = ""
            progressbar.visibility = View.VISIBLE
            if (!isLoading) {
                presenter.doRegister(name.text.toString(), email.text.toString(), password.text.toString())
                isLoading = true
            }
        }
    }

    override fun onSuccess(response: Response) {
        isLoading = false
        progressbar.visibility = View.INVISIBLE
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }

    override fun onFailure(message: String?) {
        isLoading = false
        progressbar.visibility = View.INVISIBLE
        btn_login.text = "Login"
    }

    override fun onErrors(errors: List<Error>) {
    }

}
