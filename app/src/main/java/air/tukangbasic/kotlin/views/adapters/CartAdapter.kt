package air.tukangbasic.kotlin.views.adapters

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.click
import air.tukangbasic.kotlin.helpers.toPrice
import air.tukangbasic.kotlin.models.basic.CartItem
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

/**
 * Created by tukangbasic on 09/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class CartAdapter(var isEdit: Boolean = false) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var context: Context
    var items: MutableList<CartItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        context = parent!!.context
        return Holder(LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        holder as Holder
        holder.title.text = items[position].name
        holder.price.text = "Rp. " + (items[position].total / items[position].qty).toPrice()
        holder.qty.text = items[position].qty.toString()
        Picasso.get().load(Constant.BASE_IMAGES_URL + Constant.PATH_PRODUCT + items[position].image).fit().into(holder.image)
        if (isEdit) {
            holder.edit.visibility = View.GONE
        }

    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.title)
        var image: ImageView = view.findViewById(R.id.image)
        var price: TextView = view.findViewById(R.id.price)
        var qty: TextView = view.findViewById(R.id.qty)
        var edit: ImageView = view.findViewById(R.id.edit)

        init {
            edit.click {

            }
        }
    }
}