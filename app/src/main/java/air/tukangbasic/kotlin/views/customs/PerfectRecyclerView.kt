package air.tukangbasic.kotlin.views.customs

import air.tukangbasic.kotlin.R
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.perfect_recyclerview.view.*

/**
 * Created by tukangbasic on 19/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */

/**
 * Created by tukangbasic on 17/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class PerfectRecyclerView(c: Context, attr: AttributeSet) : FrameLayout(c, attr) {

    init {
        addView(View.inflate(c, R.layout.perfect_recyclerview, null))
        progress_loading.visibility = View.GONE
        begin()
    }

    fun getIt(): RecyclerView {
        return rv
    }

    private fun begin() {
        isEmpty = false
    }

    var isEmpty: Boolean
        get() = this.isEmpty
        set(value) {
            getIt().visibility = if (value) View.GONE else View.VISIBLE
            text_message.visibility = if (value) View.VISIBLE else View.GONE
//            progress_loading.visibility = if (!value) View.VISIBLE else View.GONE

        }


    var message: String
        get() = this.toString()
        set(value) {
            text_message.text = value
        }

    var loading: Boolean
        get() = this.loading
        set(value) {
            isEmpty = value
            progress_loading.visibility = if(value) View.VISIBLE else View.GONE
            message = "Loading ..."
        }
}