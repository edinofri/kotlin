package air.tukangbasic.kotlin.views.activities

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.R.id.nav_login
import air.tukangbasic.kotlin.helpers.Constant
import air.tukangbasic.kotlin.helpers.EventBusHelper
import air.tukangbasic.kotlin.helpers.SessionManager
import air.tukangbasic.kotlin.views.fragments.BerandaFragment
import air.tukangbasic.kotlin.views.fragments.HistoryFragment
import air.tukangbasic.kotlin.views.fragments.ProfileFragment
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.greenrobot.eventbus.EventBus

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        drawerMenuLogin()
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_profile -> {
                replaceFragment(ProfileFragment(), R.id.main_container)
            }
            R.id.nav_beranda -> {
                replaceFragment(BerandaFragment(), R.id.main_container)
            }
            R.id.nav_history -> {
                replaceFragment(HistoryFragment(), R.id.main_container)
            }
            R.id.nav_logout -> {
                SessionManager(this).logout()
                drawerMenuLogin()

            }
            R.id.nav_login -> {
                startActivity(Intent(this, LoginActivity::class.java))
            }
            R.id.nav_register -> {
                startActivity(Intent(this, RegisterActivity::class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }


    fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { replace(frameId, fragment) }
    }
    fun goTo(id:Int){
        when(id) {
            R.id.nav_history ->{
                replaceFragment(HistoryFragment(), R.id.main_container)
            }
        }
    }

    fun drawerMenuLogin() {
        var menu: Menu = nav_view.menu
        EventBus.getDefault().post(EventBusHelper.AuthEvent(SessionManager(this).isLogin()));
        if (SessionManager(this).isLogin()) {
            menu.findItem(R.id.nav_login).isVisible = false
            menu.findItem(R.id.nav_register).isVisible = false
//            menu.findItem(R.id.nav_profile).isVisible = true
            menu.findItem(R.id.nav_logout).isVisible = true
            menu.findItem(R.id.nav_history).isVisible = true
        } else {
            menu.findItem(R.id.nav_login).isVisible = true
            menu.findItem(R.id.nav_register).isVisible = true
//            menu.findItem(R.id.nav_profile).isVisible = false
            menu.findItem(R.id.nav_logout).isVisible = false
            menu.findItem(R.id.nav_history).isVisible = false
        }
        replaceFragment(BerandaFragment(), R.id.main_container)
    }

    fun refreshDataBeranda() {
        EventBus.getDefault().post(EventBusHelper.RefreshBeranda())
    }

}
