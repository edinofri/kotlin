package air.tukangbasic.kotlin.helpers

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.widget.Toast
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by tukangbasic on 20/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */


fun String.toDate(pattern: String = "yyyy-MM-dd"): Date {
    var format: SimpleDateFormat = SimpleDateFormat(pattern)
    return format.parse(this)
}

fun String.toHumanDate(): String {
    var format: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var reformat: SimpleDateFormat = SimpleDateFormat("dd MMM yyyy")
    return reformat.format(format.parse(this))
}

fun Double.toPrice(separator: String = "."): String {
    return DecimalFormat("#,###").format(this).replace(",", separator)
}

fun Int.toPrice(separator: String = "."): String {
    return DecimalFormat("#,###").format(this).replace(",", separator)

}

fun View.click(listener: (View) -> Unit) {
    setOnClickListener(listener)
}

fun Int.toDP(context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics).toInt()
}
