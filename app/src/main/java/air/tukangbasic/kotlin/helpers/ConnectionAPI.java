package air.tukangbasic.kotlin.helpers;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by edinofri on 01/12/2016.
 */

public class ConnectionAPI {
    private static ConnectionAPI instance;
    private static RestService api;
    private static Retrofit retrofit;

    public static ConnectionAPI getInstance(Context context) {
        instance = new ConnectionAPI(context);
        return instance;
    }

    public RestService getApi() {
        return api;
    }

    private ConnectionAPI(final Context context) {
        Gson gson = new GsonBuilder().setLenient()
                .create();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                SessionManager sessionManager = new SessionManager(context);
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader(Constant.KEY_HEADER_AUTHORIZATION, sessionManager.isLogin() ?
                                sessionManager.getToken() : "")
                        .addHeader(Constant.KEY_HEADER_USER_AGENT, Constant.APP_NAME);
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.Companion.getBASE_API_URL())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();

        api = retrofit.create(RestService.class);
    }
}