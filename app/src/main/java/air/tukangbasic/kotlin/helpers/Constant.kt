package air.tukangbasic.kotlin.helpers

import android.content.Context

/**
 * Created by root on 16/04/18.
 */
class Constant {
    companion object {
        const val APP_NAME = "order-paper"

        //        var BASE_URL = "http://192.168.1.71:81/"
//var BASE_URL = "http://192.168.66.1:81/"
        private var BASE_URL = "http://192.168.43.141:81/"
        val BASE_API_URL = BASE_URL + "api/"
        var BASE_IMAGES_URL = BASE_URL + "images/"

        var HTTP_HEADER_SIGN = "cisabgnakut"

        const val KEY_HEADER_USER_AGENT = "User-Agent"
        const val KEY_HEADER_AUTHORIZATION = "Authorization"

        const val PREF_NAME = "kotlin-order.paper"
        const val PREF_MODE = Context.MODE_PRIVATE

        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
        const val KEY_PASSWORD = "password"
        const val KEY_DATA = "data"

        const val KEY_USER = "user"
        const val KEY_TOKEN = "token"

        var PATH_PRODUCT = "product/"
        const val TAG_PRODUCT_DETAIL_BOTTOM_SHEET = "product_detail_bottom_sheet"
        const val REQUEST_CODE_PICK_ADDRESS = 7
        const val REQUEST_CODE_ADD_ADDRESS = 8
        const val REQUEST_CODE_ORDER_SUCCESS = 2

    }

}