package air.tukangbasic.kotlin.helpers

import air.tukangbasic.kotlin.models.basic.User
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

/**
 * Created by root on 16/04/18.
 */
class SessionManager(context: Context) {
    private var pref: SharedPreferences = context.getSharedPreferences(Constant.PREF_NAME, Constant.PREF_MODE)
    private var edit: SharedPreferences.Editor = pref.edit()

    fun setUserLogin(user: User?, token: String?) {
        edit.putString(Constant.KEY_USER, Gson().toJson(user))
        edit.putString(Constant.KEY_TOKEN, token)
        edit.commit()
    }

    fun refreshToken(token: String?) {
        edit.putString(Constant.KEY_TOKEN, token)
        edit.commit()
    }

    fun getUser(): User {
        return Gson().fromJson(pref.getString(Constant.KEY_USER, null), User::class.java)
    }

    fun getToken(): String {
        return pref.getString(Constant.KEY_TOKEN, null)
    }

    fun isLogin(): Boolean {
        return pref.getString(Constant.KEY_TOKEN, null) != null
    }

    fun logout() {
        edit.clear()
        edit.commit()
    }
}