package air.tukangbasic.kotlin.helpers

import air.tukangbasic.kotlin.models.response.*
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by root on 16/04/18.
 */

interface RestService {

    @POST("login")
    fun login(@Body data: Map<String, String>): Flowable<Response<ResponseUser>>

    @POST("register")
    fun register(@Body data: Map<String, String>): Flowable<Response<ResponseUser>>

    @GET("products")
    fun products(): Flowable<Response<ResponseProduct>>

    @POST("cart")
    fun addCart(@Body data: Map<String, String>): Flowable<Response<air.tukangbasic.kotlin.models.basic.Response>>

    @GET("cart")
    fun getCart(): Flowable<Response<ResponseCart>>

    @POST("checkout")
    fun checkout(@Body data: Map<String, String>): Flowable<Response<air.tukangbasic.kotlin.models.basic.Response>>

    @POST("confirm")
    fun confirm(@Body data: Map<String, String>): Flowable<Response<air.tukangbasic.kotlin.models.basic.Response>>

    @POST("address")
    fun addAddress(@Body data: Map<String, String>): Flowable<Response<air.tukangbasic.kotlin.models.basic.Response>>

    @GET("address")
    fun getAddress(): Flowable<Response<ResponseAddress>>

    @GET("history")
    fun getHistory(): Flowable<Response<ResponseHistory>>
}