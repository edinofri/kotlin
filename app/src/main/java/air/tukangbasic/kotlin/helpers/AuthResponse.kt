package air.tukangbasic.kotlin.helpers

import air.tukangbasic.kotlin.models.basic.Error

/**
 * Created by root on 16/04/18.
 */
interface  AuthResponse:BaseResponse{
    fun onErrors(errors:List<Error>)
}