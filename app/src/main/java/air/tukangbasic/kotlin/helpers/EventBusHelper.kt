package air.tukangbasic.kotlin.helpers

import air.tukangbasic.kotlin.models.basic.Product

/**
 * Created by tukangbasic on 15/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class EventBusHelper {
    class AuthEvent(isLogin: Boolean)
    class RefreshBeranda
}