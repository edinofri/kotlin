package air.tukangbasic.kotlin.helpers

import air.tukangbasic.kotlin.models.basic.Response

/**
 * Created by root on 16/04/18.
 */
interface BaseResponse {
    fun onSuccess(response:Response)
    fun onFailure(message:String?)
}