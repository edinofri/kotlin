package air.tukangbasic.kotlin.models.basic

/**
 * Created by tukangbasic on 15/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
open class Product(var id: Int,
              var name: String,
              var price: String,
              var description:String,
              var category_id: Int,
              var stock: Int,
              var image: String,
              var deleted_at: String,
              var created_at: String,
              var category: Category) {
    class Category(var id: Int, var name: String, var description: String)
}