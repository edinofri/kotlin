package air.tukangbasic.kotlin.models.basic

/**
 * Created by tukangbasic on 11/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class Cart {
    var items:List<CartItem> = ArrayList()
}