package air.tukangbasic.kotlin.models.basic

/**
 * Created by tukangbasic on 15/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
open class Pagination {

    var total: Number? = null
    var per_page: Number? = null
    var current_page: Number? = null
    var last_page: Number? = null
    var from: Number? = null
    var to: Number? = null
}