package air.tukangbasic.kotlin.models.response

import air.tukangbasic.kotlin.models.basic.CartItem
import air.tukangbasic.kotlin.models.basic.Response

class ResponseCart : Response() {
    var data: MutableList<CartItem>? = mutableListOf()

}
