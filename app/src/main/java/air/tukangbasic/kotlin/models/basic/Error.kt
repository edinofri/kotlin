package air.tukangbasic.kotlin.models.basic

/**
 * Created by root on 16/04/18.
 */

open class Error {
    var message: String? = null
    var tag: String? = null
}