package air.tukangbasic.kotlin.models.response

import air.tukangbasic.kotlin.models.basic.Pagination
import air.tukangbasic.kotlin.models.basic.Product
import air.tukangbasic.kotlin.models.basic.Response

/**
 * Created by tukangbasic on 15/05/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
open class ResponseProduct: Response() {
    var data:ResponseData? = null

    class ResponseData: Pagination(){
        var data:List<Product>? = ArrayList()
    }
}