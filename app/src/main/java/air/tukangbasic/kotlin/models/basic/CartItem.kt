package air.tukangbasic.kotlin.models.basic

/**
 * Created by tukangbasic on 09/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class CartItem(var id: Int, var cart_id: Int, var product_id: Int, var qty: Int, var total: Double,var name:String,var stock:Int,var image:String)