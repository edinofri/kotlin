package air.tukangbasic.kotlin.models.response

import air.tukangbasic.kotlin.models.basic.Address
import air.tukangbasic.kotlin.models.basic.Response

/**
 * Created by tukangbasic on 10/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class ResponseAddress : Response() {
    var data: MutableList<Address> = mutableListOf()
}