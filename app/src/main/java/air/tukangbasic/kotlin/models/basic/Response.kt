package air.tukangbasic.kotlin.models.basic

/**
 * Created by root on 16/04/18.
 */
open class Response {
    var message: String? = null
    var success: Boolean = false
}