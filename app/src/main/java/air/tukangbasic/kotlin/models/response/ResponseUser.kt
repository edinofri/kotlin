package air.tukangbasic.kotlin.models.response

import air.tukangbasic.kotlin.models.basic.Response
import air.tukangbasic.kotlin.models.basic.User

/**
 * Created by root on 16/04/18.
 */
open class ResponseUser : Response() {
    var data: ResponseData? = null

    inner class ResponseData {
        var user: User? = null
        var token: String? = null
    }
}