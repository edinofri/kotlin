package air.tukangbasic.kotlin.models.basic

/**
 * Created by tukangbasic on 10/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class Address(val id: Int, val user_id: Int, val receiver_name: String, val phone_number: String,
              val provinsi: String, val kabupaten: String, val kecamatan: String, val alamat: String,
              val default: Int, val geo: String)