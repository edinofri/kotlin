package air.tukangbasic.kotlin.models.basic

import air.tukangbasic.kotlin.R
import air.tukangbasic.kotlin.helpers.Constant
import android.content.Context
import android.graphics.drawable.Drawable

/**
 * Created by tukangbasic on 10/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class History(val id: Int,
              val invoice: String,
              val status: Int,
              val address: Address,
              val cart: Cart,
              var created_at: String,
              var updated_at: String
) {

    fun getStatus(): String {
        return when (status) {
            -1 -> "Cancel"
            0 -> "Wait Confirmation"
            1 -> "Package on delivery"
            2 -> "Package Received"
            else -> {
                ""
            }
        }
    }

    fun getColorByStatus(): Int {
        return when (status) {
            -1 -> R.color.cancel
            0 -> R.color.wait
            1 -> R.color.delivery
            2 -> R.color.receive
            else -> {
                R.color.wait
            }
        }
    }

    private fun getResourceByStatus(): Int {
        return when (status) {
            -1 -> R.drawable.shape_cancel
            0 -> R.drawable.shape_wait
            1 -> R.drawable.shape_delivery
            2 -> R.drawable.shape_receive
            else -> {
                R.drawable.shape_wait
            }
        }
    }

    fun getDrawableByStatus(context: Context): Drawable? {
        return context.resources.getDrawable(getResourceByStatus())
    }

}