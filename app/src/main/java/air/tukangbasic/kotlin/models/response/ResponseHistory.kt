package air.tukangbasic.kotlin.models.response

import air.tukangbasic.kotlin.models.basic.History
import air.tukangbasic.kotlin.models.basic.Pagination
import air.tukangbasic.kotlin.models.basic.Response

/**
 * Created by tukangbasic on 11/06/18.
 * Mail:tukangbasic@gmail.com
 * git: gitlab.com/edinofri, github.com/edinofricaniago
 */
class ResponseHistory:Response() {
    var data:ResponseData? = null
    class ResponseData:Pagination(){
        var data:List<History>? = ArrayList()
    }
}